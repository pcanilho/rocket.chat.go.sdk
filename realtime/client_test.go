package realtime

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pcanilho/rocket.chat.go.sdk/common_testing"
	"gitlab.com/pcanilho/rocket.chat.go.sdk/models"
)

var (
	client *Client
)

func getLoggedInClient(t *testing.T) *Client {

	if client == nil {
		c, err := NewClient(&url.URL{Host: common_testing.Host + ":" + common_testing.Port}, true)
		assert.Nil(t, err, "Couldn't create realtime client")

		_, err = c.RegisterUser(&models.UserCredentials{
			Email:    common_testing.GetRandomEmail(),
			Name:     common_testing.GetRandomString(),
			Password: common_testing.GetRandomString()})
		assert.Nil(t, err, "Couldn't register user")

		client = c
	}

	return client
}
