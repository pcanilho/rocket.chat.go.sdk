package rest

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pcanilho/rocket.chat.go.sdk/models"
)

func TestRocket_SendAndReceive(t *testing.T) {
	rocket := getDefaultClient(t)
	general := &models.Channel{ID: "GENERAL", Name: "general"}

	err := rocket.Send(general, "Test")
	assert.Nil(t, err)

	messageChannelType := models.MessageFromChannel
	messages, err := rocket.GetMessages(general, &messageChannelType, &models.Pagination{Count: 10})
	assert.Nil(t, err)

	message := findMessage(messages, testUserName, "Test")
	assert.NotNil(t, message)
}
