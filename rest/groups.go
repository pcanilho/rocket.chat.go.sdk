package rest

import (
	"gitlab.com/pcanilho/rocket.chat.go.sdk/models"
)
type GroupsResponse struct {
	Status
	models.Pagination
	Groups []models.Group `json:"groups"`
}

type GroupResponse struct {
	Status
	Group models.Group `json:"group"`
}

// Lists all of the private groups the calling user has joined.
//
// https://rocket.chat/docs/developer-guides/rest-api/groups/list
func (c *Client) GetJoinedGroups() (*GroupsResponse, error) {
	response := new(GroupsResponse)
	if err := c.Get("groups.list", nil, response); err != nil {
		return nil, err
	}

	return response, nil
}
